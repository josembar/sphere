package sphere;
/**
 *
 * @author Jose
 */
public class Main {

    public static void main(String[] args) {
        
        Sphere sphere1 = new Sphere(5);
        System.out.println(sphere1.getDiameter());
        System.out.println(sphere1.getCircumference());
        System.out.println(sphere1.getSurfaceArea());
        System.out.println(sphere1.getVolume());
        System.out.println();
        Sphere sphere2 = new Sphere(8,"m");
        System.out.println(sphere2.getDiameter());
        System.out.println(sphere2.getCircumference());
        System.out.println(sphere2.getSurfaceArea());
        System.out.println(sphere2.getVolume());
    }
    
}
