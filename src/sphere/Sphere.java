
package sphere;

public class Sphere {
    
    private double radius;
    private String unit;
    private static final String squareWord = "square";
    private static final String cubicWord = "cubic";

    public Sphere(double radius) 
    {
        this.radius = radius;
        this.unit = "cm";
    }
    
    public Sphere(double radius, String unit) 
    {
        this.radius = radius;
        this.unit = unit;
    }

    public double getRadius() 
    {
        return radius;
    }

    public void setRadius(double radius) 
    {
        this.radius = radius;
    }

    public String getUnit() 
    {
        return unit;
    }

    public void setUnit(String unit) 
    {
        this.unit = unit;
    }
    
    //get the diameter of the sphere
    public String getDiameter()
    {
        return (this.getRadius() * 2) + " " + this.getUnit();
    }
    
    //get the circumference of the sphere
    public String getCircumference()
    {
        return (2 * Math.PI * this.getRadius()) + " " + this.getUnit();
    }
    
    //get the surface area of the sphere
    public String getSurfaceArea()
    {
        return (4 * Math.PI * Math.pow(this.getRadius(), 2)) + " " + squareWord + " " + this.getUnit();
    }
    
    //get the volume of the sphere
    public String getVolume()
    {
        return (((4 * Math.PI) / 3) * Math.pow(this.getRadius(), 3)) + " " + cubicWord + " " + this.getUnit();
    }
}
